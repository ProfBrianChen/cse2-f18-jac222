//Alexander Carr CSE 002
//17 November 2018
//CSE2Linear is a program that searches an array of ascending grades using a binary search, then shuffles the grades and searches them linearly.

import java.util.*;

public class CSE2Linear {
	
	public static void main (String[] args) {
		
		//Declaring and initializing variables
		Scanner sc = new Scanner(System.in);
		int lastInt = 0;
		int search;
		int[] grades = new int[15];
		boolean checkType = false;
		
		//Asking the user for an input 15 times
		System.out.println("Enter 15 ascending ints for final grades in CSE2:");
		for (int i = 0; i < 15; i++) {
			
			//Checking the user's input for an integer
			do {
				
				//Checking if the user entered an int
				checkType = sc.hasNextInt();
				
				//If the user enters an int
				if (checkType == true) {
					
					//Assigning the int a position in the array
					grades[i] = sc.nextInt();
					
					//Checking if the int is between 0 and 100
					if (grades[i] >= 0 && grades[i] <= 100) {
						
						//Checking if the int is greater than the last int
						if (grades[i] >= lastInt) {
							
							//Resetting the last int
							lastInt = grades[i];
							checkType = true;
							
						}
						
						//If the int is not greater than the last int
						else {
							
							//Prompting the user for another input
							System.out.println("Error: Input must be greater than previous input");
							checkType = false;
							
						}
						
					}
					
					//If the int is not between 0 and 100
					else {
						
						//Prompting the user for another input
						System.out.println("Error: Input must be between 0 and 100");
						checkType = false;
						
					}
					
				}
				
				//If the user did not enter an int
				else {
					
					//Prompting the user for another int
					System.out.println("Error: Invalid input, please enter an integer");
					sc.nextLine();
					
				}
				
			} while (checkType == false);
			
			//Resetting checkType
			checkType = false;
			
		}
		
		//Printing the array
		print(grades);
		
		//Prompting the user for a grade to be searched for
		System.out.print("Enter a grade to search for: ");
		search = sc.nextInt();
		
		//Searching the array
		binarySearch(grades, search);
		
		//Scrambling the array
		scramble(grades);
		print(grades);
		
		//Prompting the user for a grade to be searched for
		System.out.print("Enter a grade to search for: ");
		search = sc.nextInt();
		
		//Searching the array
		linearSearch(grades, search);
		
		
		
	} //End of main method
	
	//Used Professor Carr's code that we created in class as a guide
	public static void linearSearch(int[] array, int value) {
		
		//Declaring and initializing variables
		int counter = 0;
		boolean found = false;
		
		//Searching the array
		for (int i = 0; i < array.length; i++) {
			
			//Checking if the array value is equal to the search value
			if (value == array[i]) {
				
				//Printing that the value was found
				System.out.println("The grade was found in the list with " + counter + " iterations");
				found = true;
				break;
				
			}
			
			//If the array value is not equal to the search value
			else {
				
				//Incrementing the counter
				counter++;
				
			}
			
		}
		
		//If the value was not found
		if (found == false) {
					
			//Outputting that the value was not found
			System.out.println(value + " was not found in the list with " + counter + " iterations");
					
		}
		
	} //End of linearSearch method
	
	//Used Professor Carr's code that we created in class as a guide
	public static void binarySearch(int[] array, int value) {
		
		//Declaring and initializing variables
		int midBound, lowBound, upBound;
		int counter = 0;
		boolean found = false;
		
		//Setting the bounds
		lowBound = 0;
		upBound = array.length - 1;
		
		//Searching the array
		while (lowBound <= upBound) {
			
			//Finding the middle of the bounds
			midBound = (upBound + lowBound) / 2;
			
			//Checking if the value is less than the middle
			if (value < array[midBound]) {
				
				//Setting a new upper bound
				upBound = midBound - 1;
				counter++;
				
			}
			
			//Checking if the value is equal to the middle
			else if (value == array[midBound]) {
				
				//Printing that the grade was found
				System.out.println("The grade was found in the list with " + counter + " iterations");
				found = true;
				break;
				
			}
			
			//Checking if the value is greater than the middle
			else {
				
				//Setting a new lower bound
				lowBound = midBound + 1;
				counter++;
				
			}
			
		}
		
		//If the value was not found
		if (found == false) {
			
			//Outputting that the value was not found
			System.out.println(value + " was not found in the list with " + counter + " iterations");
			
		}
		
	} //End of binarySearch method
	
	public static void scramble(int[] array) {
		
		//Declaring and initializing variables
		int random, randPos;
		int temp = 0;
		
		//Finding a random number of times to shuffle the cards
    	random = (int)(Math.random() * 1000) + 50;
    	
    	//Using a for loop to swap values of the array
    	for (int i = 0; i < random; i++) {
    		
    		//Finding a random position to switch
    		randPos = (int)(Math.random() * array.length);
    		
    		//Using a temp value to switch the two values of the array
    		temp = array[0];
    		array[0] = array[randPos];
    		array[randPos] = temp;
    		
    	}
    	
    	//Printing out scrambled
    	System.out.println("Scrambled");
		
	} //End of shuffle method
	
	public static void print(int[] a) {
		
		//Using a for loop to print the array
		for (int i = 0; i < a.length - 1; i++) {
			
			//Printing the array
			System.out.print(a[i] + " ");
			
		}
		
		//Printing the last value of the array
		System.out.println(a[a.length - 1]);
		
	} //End of print method

}
