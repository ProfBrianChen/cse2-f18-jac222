//Alexander Carr CSE 002
//17 November 2018
//RemoveElements is a program that removes specific elements from a randomly generated array of 10 integers.

import java.util.*;

public class RemoveElements {

	public static void main(String [] arg){
		
		//Declaring and initializing variables
		Scanner scan = new Scanner(System.in);
		int num[] = new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		String answer = "";
		
		do{
			
			//Randomly generating 10 ints
			System.out.print("Random input 10 ints [0-9]");
		    num = randomInput();
		    String out = "The original array is:";
		    out += listArray(num);
		    System.out.println(out);
		 
		    //Deleting the number at the given index
		    System.out.print("Enter the index ");
		    index = scan.nextInt();
		    newArray1 = delete(num, index);
		    String out1 = "The output array is ";
		    out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
		    System.out.println(out1);
		 
		    //Deleting all instances of a target value
		    System.out.print("Enter the target value ");
		    target = scan.nextInt();
		    newArray2 = remove(num,target);
		    String out2 = "The output array is ";
		    out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
		    System.out.println(out2);
		       
		    //Asking the user to go again
		    System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
		    answer = scan.next();
		    
		} while (answer.equals("Y") || answer.equals("y"));

	} //End of main method
	
	public static int[] remove(int[] array, int value) {
		
		//Declaring and initializing variables
		int counter = 0;
		int subtract = 0;
		
		//Using a for loop to cycle through the array
		for (int i = 0; i < array.length; i++) {
			
			//Checking if the number is equal to the target value
			if (array[i] == value) {
				
				//Incrementing the counter
				counter++;
				
			}
			
		}
		
		if (counter == 0) {
			
			//Outputting an error statement and returning the old array
			System.out.println("Element " + value + " was not found");
			return array;
			
		}
		
		else {
		
			//Creating a new output array
			int[] output = new int[array.length - counter];
		
			//Cycling through each of the positions in the array
			for (int j = 0; j < array.length; j++) {
			
				//Checking if the value is equal to the target value
				if (array[j] == value) {
				
					//Incrementing the subtractor
					subtract++;
				
				}
			
				//If the value is not equal to the target value
				else {
				
					//Copying the array over to an output array
					output[j - subtract] = array[j];
				
				}
			
			}
		
			//Notifying the user that the target value was found and returning the output
			System.out.println("Element " + value + " has been found");
			return output;
			
		}
		
	} //End of remove method
	
	public static int[] delete(int[] array, int pos) {
		
		//Declaring and initializing variables
		int[] output = new int[array.length - 1];
		
		//Checking if the position is in the bounds of the array
		if (pos >= 0 && pos <= array.length - 1) {
		
			//Using a for loop to cycle through the arrays
			for (int i = 0; i < array.length; i++) {
			
				//Checking if the i is less than the deleted position
				if (i < pos) {
				
					//Copying array to output
					output[i] = array[i];
				
				}
			
				//Checking if the i is greater than the deleted position
				else if (i > pos) {
				
					//Copying array to output
					output[i - 1] = array[i];
				
				}
			
			}
			
			//Notifying the user that the element was removed and returning the output
			System.out.println("Index " + pos + " element is removed");
			return output;
		
		}
		
		//If the position is not within the bounds
		else {
			
			//Outputting an error statement and returning the old array
			System.out.println("The index is not valid");
			return array;
			
		}
		
	}
	
	public static int[] randomInput() {
		
		//Declaring and initializing variables
		int[] array = new int[10];
		
		//Cycling through each of the positions in the array
		for (int i = 0; i < 10; i++) {
			
			//Assigning a random int to each position in the array
			int random = (int)(Math.random() * 10);
			array[i] = random;
			
		}
		
		//Returning the output
		return array;
		
	} //End of randomInput method
		 
	public static String listArray(int num[]){
		
		//Declaring and initializing variables
		String out = "{";
		
		//Using a for loop to run through the positions in the arrray
		for(int j=0;j<num.length;j++){
			
			//Checking if the position is greater than 0
			if (j > 0) {
				
				//Adding a comma in between poitions
				out += ", ";
		    
			}
			
			//Adding the next number in num to the output
		    out += num[j];
		    
		}
		    
		    //Returning the output
		    out += "} ";
		    return out;
	} //End of listArray method

}