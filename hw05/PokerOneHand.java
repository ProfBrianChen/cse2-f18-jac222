//Alexander Carr CSE 002
//October 4 2018
//PokerOneHand is a program that randomly generates hands for poker and determines the probability of pairs based on its results

import java.util.Scanner;

public class PokerOneHand {
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		Scanner sc = new Scanner(System.in);
		int temp, cardCount;
		int card1 = 0, card2 = 0, card3 = 0, card4 = 0, card5 = 0;
		int handsGen = 0, threeOfKind = 0, fourOfKind = 0, onePair = 0, twoPair = 0;
		double threeOfKindOut = 0, fourOfKindOut = 0, onePairOut = 0, twoPairOut = 0;
		boolean checkType;
		
		//Asking the user how many times the computer should generate hands
		System.out.println("How many times should the computer generate hands?");
		
		//Using a do while loop to continually check for the correct input
	    do {
	      
	    	//Checking that the user inputs the correct type
	    	checkType = sc.hasNextInt();
	      
	    	//Checking if the input is an int
	    	if (checkType == true) {
	        
	    		//Obtaining the correct amount of hands
	    		handsGen = sc.nextInt();
	        
	    	}
	      
	    	//Checking if the input is not an int
	    	if (checkType == false) {
	        
	    		//Clearing the incorrect input and prompting the user for another input
	    		sc.next();
	    		System.out.println("Error: Invalid Input\nPlease enter the amount of hands as an int:");
	        
	    	}
	    	
	    } while (checkType == false);
	    
	    //Creating the big loop that will run the calculations for each hand
	    int i = 0;
	    while (i < handsGen) {
	    	
	    	//Randomly selecting the first card
	    	card1 = (int)(Math.random() * 52) + 1;
	    	
	    	//Using a loop to make sure the second card is not a duplicate
	    	do {
	    		
	    		//Randomly selecting the second card
	    		card2 = (int)(Math.random() * 52) + 1;
	    		
	    	} while (card1 == card2);
	    	
	    	//Using a loop to make sure the third card is not a duplicate
	    	do {
	    		
	    		//Randomly selecting the third card
	    		card3 = (int)(Math.random() * 52) + 1;
	    		
	    	} while (card1 == card3 || card2 == card3);
	    	
	    	//Using a loop to make sure the fourth card is not a duplicate
	    	do {
	    		
	    		//Randomly selecting the fourth card
	    		card4 = (int)(Math.random() * 52) + 1;
	    		
	    	} while (card1 == card4 || card2 == card4 || card3 == card4);
	    	
	    	//Using a loop to make sure the fifth card is not a duplicate
	    	do {
	    		
	    		//Randomly selecting the fifth card
	    		card5 = (int)(Math.random() * 52) + 1;
	    		
	    	} while (card1 == card5 || card2 == card5 || card3 == card5 || card4 == card5);
	    	
	    	//Using the mod function to change all of the cards to numbers 0 - 12, regardless of suit
	    	card1 = card1 % 13;
	    	card2 = card2 % 13;
	    	card3 = card3 % 13;
	    	card4 = card4 % 13;
	    	card5 = card5 % 13;
	    	
	    	//Using a temporary place holder that will be used to check for a two pair after the loop
	    	temp = onePair;
	    	
	    	//Using a loop to check how many of each card is in the hand
	    	int j = 0;
	    	while (j < 13) {
	    		
	    		//Setting the card counter to 0 for each time through the loop
	    		cardCount = 0;
	    		
	    		//Checking the first card
	    		if (j == card1) {
	    			
	    			//Incrementing the card counter
	    			cardCount++;
	    			
	    		}
	    		
	    		//Checking the second card
	    		if (j == card2) {
	    			
	    			//Incrementing the card counter
	    			cardCount++;
	    			
	    		}
	    		
	    		//Checking the third card
	    		if (j == card3) {
	    			
	    			//Incrementing the card counter
	    			cardCount++;
	    			
	    		}
	    		
	    		//Checking the fourth card
	    		if (j == card4) {
	    			
	    			//Incrementing the card counter
	    			cardCount++;
	    			
	    		}
	    		
	    		//Checking the fifth card
	    		if (j == card5) {
	    			
	    			//Incrementing the card counter
	    			cardCount++;
	    			
	    		}
	    		
	    		//Checking if there is a pair
	    		if (cardCount == 2) {
	    			
	    			//Incrementing the one pair counter
	    			onePair++;
	    			
	    		}
	    		
	    		//Checking if there is a three of a kind
	    		if (cardCount == 3) {
	    			
	    			//Incrementing the three of a kind counter
	    			threeOfKind++;
	    			
	    		}
	    		
	    		//Checking if there is a four of a kind
	    		if (cardCount == 4) {
	    			
	    			//Incrementing the four of a kind counter
	    			fourOfKind++;
	    			
	    		}
	    		
	    		//Incrementing the loop counter
	    		j++;
	    		
	    	}
	    	
	    	//Checking if there is a two pair
	    	if (onePair - temp == 2) {
	    		
	    		//Incrementing the two pair counter and adjusting the one pair counter
	    		onePair -= 2;
	    		twoPair++;
	    		
	    	}
	    	
	    	//Incrementing the loop counter
	    	i++;
	    	
	    }
	    
	    //Finding the probability of each type of winning hand
	    fourOfKindOut = ((double) fourOfKind) / handsGen;
	    threeOfKindOut = ((double) threeOfKind) / handsGen;
	    twoPairOut = ((double) twoPair) / handsGen;
	    onePairOut = ((double) onePair) / handsGen;
	    
	    //Outputting the results of the program
	    System.out.println("The number of loops: " + handsGen);
	    System.out.printf("The probability of Four-of-a-kind: %1.3f\n", fourOfKindOut);
	    System.out.printf("The probability of Three-of-a-kind: %1.3f\n", threeOfKindOut);
	    System.out.printf("The probability of Two-pair: %1.3f\n", twoPairOut);
	    System.out.printf("The probability of One-pair: %1.3f\n", onePairOut);
	    
	}

}