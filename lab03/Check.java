//Alexander Carr CSE 002
//Sep 13 2018
//Check is a program that takes the total cost of a check, including tax and tip, and splits it evenly

import java.util.Scanner;

public class Check {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Obtaining the cost of the check
    double checkCost = myScanner.nextDouble();
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //Obtaining the wanted tip percentage
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;  //Turning the tip into a decimal
    
    System.out.print("Enter the number of people who went out to dinner: "); //Obtaining the number of people that were at dinner
    double numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    
    totalCost = checkCost * (1 + tipPercent); //Finding total cost including tip
    costPerPerson = totalCost / numPeople; //Finding the cost per person
    dollars = (int) costPerPerson; //Finding the dollars, dimes, and pennies using remainders to create the cost for each person
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //Outputting the final cost per person
    
  } //End of main method
  
} //End of class