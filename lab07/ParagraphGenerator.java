//Alexander Carr CSE 002
//25 October 2018
//ParagraphGenerator is a program that randomly generates sentences to create a paragraph.

import java.util.Scanner;
import java.util.Random;

public class ParagraphGenerator {
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		Scanner sc = new Scanner(System.in);
		boolean checkType = false;
		int sentences = 0;
		String subject, object;
		
		//Prompting the user for the number of sentences
		System.out.println("Please enter an integer for the number of sentences you would like in your paragraph (at least 2):");
		do {
			
			//Checking if the user inputs the correct type
			checkType = sc.hasNextInt();
			
			//Checking if the input is an integer
			if (checkType == true) {
				
				//Obtaining the correct amount of sentences
				sentences = sc.nextInt();
				
				//Checking if the integer is greater than 2
				if (sentences >= 2) {
					
					//Exiting the loop
					break;
					
				}
				
				//If the integer is not greater than 2
				else {
					
					//Continuing the loop
					checkType = false;
					
				}
				
			}
			
			//Checking if the user inputs the correct type
			if (checkType == false) {
				
				//Clearing the incorrect input and prompting the user for a new input
				sc.nextLine();
				System.out.println("Error: Invalid Input\nPlease enter the amount of sentences as an int:");
				
			}
			
		} while (checkType == false);
		
		//Creating the subject and the object
		subject = SubjectNoun(Random());
		object = ObjectNoun(Random());
				
		//Creating the first sentence of the paragraph
		System.out.println("The" + Adjective(Random()) + Adjective(Random()) + subject + Verb(Random()) + " the" + Adjective(Random()) + object + ".");
		
		//Checking if the user wants more than 2 sentences
		if (sentences > 2) {
			
			//Using a for loop to generate more random sentences
			for (int i = 0; i < sentences - 2; i++) {
				
				//Creating 2 sentence formats
				if (i % 2 == 0) {
					
					//Creating a random sentence
					System.out.println("This" + subject + " was" + Adjective(Random()) + " to" + Verb(Random()) + ObjectNoun(Random()) + ".");
					
				}
				
				//Creating 2 sentence formats
				if (i % 2 == 1) {
					
					//Creating a random sentence
					System.out.println("It" + " used" + ObjectNoun(Random()) + " to" + Verb(Random()) + ObjectNoun(Random()) + " at the" + Adjective(Random()) + ObjectNoun(Random()) + ".");
					
				}
				
			}
			
		}
		
		//Creating the last sentences of the paragraph
		System.out.println(Conclusion(subject, object));
		
	} //End of main method
	
	public static int Random() {
		
		//Declaring and initializing variables
		Random ran = new Random();
		int output;
		
		//Generating a random integer between 0 and 9
		output = ran.nextInt(10);
		
		//Returning the output
		return output;
		
	} //End of Random method
	
	public static String Adjective(int num) {
		
		//Declaring and initializing variables
		String output = "";
		
		//Using a switch statement to select a random adjective
		switch (num) {
		
		case 0:
			//Random output
			output = " capable";
		break;
		case 1:
			//Random output
			output = " similar";
		break;
		case 2:
			//Random output
			output = " impossible";
		break;
		case 3:
			//Random output
			output = " helpful";
		break;
		case 4:
			//Random output
			output = " impressive";
		break;
		case 5:
			//Random output
			output = " desperate";
		break;
		case 6:
			//Random output
			output = " wonderful";
		break;
		case 7:
			//Random output
			output = " willing";
		break;
		case 8:
			//Random output
			output = " traditional";
		break;
		case 9:
			//Random output
			output = " practical";
		break;
		
		}
		
		//Returning the output
		return output;
		
	} //End of Adjective method
	
	public static String SubjectNoun(int num) {
		
		//Declaring and initializing variables
		String output = "";
		
		//Using a switch statement to select a random noun
		switch (num) {
		
		case 0:
			//Random output
			output = " bear";
		break;
		case 1:
			//Random output
			output = " fox";
		break;
		case 2:
			//Random output
			output = " dog";
		break;
		case 3:
			//Random output
			output = " cat";
		break;
		case 4:
			//Random output
			output = " lion";
		break;
		case 5:
			//Random output
			output = " fish";
		break;
		case 6:
			//Random output
			output = " bird";
		break;
		case 7:
			//Random output
			output = " tiger";
		break;
		case 8:
			//Random output
			output = " giraffe";
		break;
		case 9:
			//Random output
			output = " elephant";
		break;
		
		}
		
		//Returning the output
		return output;
		
	} //End of SubjectNoun method
	
	public static String ObjectNoun(int num) {
		
		//Declaring and initializing variables
		String output = "";
		
		//Using a switch statement to select a random noun
		switch (num) {
		
		case 0:
			//Random output
			output = " hotel";
		break;
		case 1:
			//Random output
			output = " country";
		break;
		case 2:
			//Random output
			output = " community";
		break;
		case 3:
			//Random output
			output = " lake";
		break;
		case 4:
			//Random output
			output = " church";
		break;
		case 5:
			//Random output
			output = " population";
		break;
		case 6:
			//Random output
			output = " classroom";
		break;
		case 7:
			//Random output
			output = " store";
		break;
		case 8:
			//Random output
			output = " city";
		break;
		case 9:
			//Random output
			output = " computer";
		break;
		
		}
		
		//Returning the output
		return output;
		
	} //End of ObjectNoun method
	
	public static String Verb(int num) {
		
		//Declaring and initializing variables
		String output = "";
		
		//Using a switch statement to select a random verb
		switch (num) {
		
		case 0:
			//Random output
			output = " dressed";
		break;
		case 1:
			//Random output
			output = " swam";
		break;
		case 2:
			//Random output
			output = " pretended";
		break;
		case 3:
			//Random output
			output = " ceased";
		break;
		case 4:
			//Random output
			output = " suffered";
		break;
		case 5:
			//Random output
			output = " inspired";
		break;
		case 6:
			//Random output
			output = " stored";
		break;
		case 7:
			//Random output
			output = " reversed";
		break;
		case 8:
			//Random output
			output = " played";
		break;
		case 9:
			//Random output
			output = " manipulated";
		break;
		
		}
		
		//Returning the output
		return output;
		
	} //End of Verb method
	
	public static String Conclusion(String subject, String object) {
		
		//Declaring and initializing variables
		String output = "";
		
		//Creating the conclusion sentence
		output = "That" + subject + Verb(Random()) + " its" + object + "!";
		
		//Returning the output
		return output;
		
	}

} //End of class