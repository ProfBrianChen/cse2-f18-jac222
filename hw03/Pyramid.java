//Alexander Carr CSE 002
//September 14 2018
//Pyramid is a program that receives the dimensions for a pyramid and outputs the volume of the pyramid

import java.util.Scanner;

public class Pyramid {
  
  public static void main(String[] args) {
    
    //Declaring my scanner and other variables
    Scanner myScanner = new Scanner(System.in);
    double pyramidVolume;
    double baseArea;
    
    //Inputting the length of the pyramid
    System.out.print("The sqaure side of the pyramid is (input length): ");
    double sideLength = myScanner.nextDouble();
    
    //Inputting the height of the pyramid
    System.out.print("The height of the pyramid is (input height): ");
    double pyramidHeight = myScanner.nextDouble();
    
    //Calculating the volume of the pyramid
    baseArea = Math.pow(sideLength, 2);
    pyramidVolume = (1.0 / 3.0) * baseArea * pyramidHeight;
    
    //Outputting the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + pyramidVolume);
    
  }
  
}