//Alexander Carr CSE 002
//Sep 14 2018
//Convert is a program that takes the number of acres of land and inches of rainfall and converts it into cubic miles of water

import java.util.Scanner;

public class Convert {
  
  public static void main(String[] args) {
    
    //Declaring my scanner and other variables
    double cubicMiles;
    double inchesToMiles;
    double acresToMiles;
    Scanner myScanner = new Scanner(System.in);
    
    //Inputting the amount of acres affected by the rain
    System.out.print("Enter the affected area in acres: ");
    double acresAffected = myScanner.nextDouble();
    
    //Inputting the amount of rainfall in that area in inches
    System.out.print("Enter the rainfall in the affected area: ");
    double inchesRainfall = myScanner.nextDouble();
    
    //Converting both the inches of rain and acres of land to miles
    inchesToMiles = inchesRainfall * (1.0 / 12.0) * (1.0 / 5280.0);
    acresToMiles = acresAffected * (1.0 / 640.0);
    
    //Multiplying the miles of rainfall and miles of land to find cubic miles of rain
    cubicMiles = inchesToMiles * acresToMiles;
    
    //Outputting the total amount of cubic miles of rain
    System.out.println("There was a total of " + cubicMiles + " cubic miles of rainfall.");
        
  }
  
}