//Alexander Carr CSE 002
//1 week
//CrapsIf is a program that either randomly chooses two die rolls or receives input for two die rolls and outputs the unique Craps name for the die combination (using only if statements)

import java.util.Scanner;

public class CrapsSwitch {
  
  public static void main(String[] args) {
    
    //Initializing scanner
    Scanner sc = new Scanner(System.in);
    
    //Declaring variables
    String input;
    int dice1;
    int dice2;
    
    //Obtaining the input for either a random roll or a user generated roll
    System.out.println("Would you like to roll the die? (Enter 'yes' or 'no')");
    input = sc.nextLine();
    
    //Checking if the user chose to roll the die themselves
    switch (input) {
      
      case "yes":
      
        //Obtaining the input of the two die from the user
        System.out.println("Ok, what 2 numbers did you roll? (Enter a number between 1 and 6 for each roll)");
        dice1 = sc.nextInt();
        dice2 = sc.nextInt();
        
        //Checking if dice1 is a valid roll
        switch (dice1) {
            
          case 1:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled a Snake Eyes.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled an Ace Deuce.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Four.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled a Fever Five.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Six.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
          
          case 2:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled an Ace Deuce.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled a Hard Four");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled a Fever Five.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Six.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Eight.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          case 3:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled an Easy Four.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled a Fever Five.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled a Hard Six.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Eight.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled a Nine.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          case 4:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled a Fever Five,");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Six.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled a Hard Eight.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled a Nine.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Ten.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          case 5:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled an Easy Six.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Eight.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled a Nine.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled a Hard Ten.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled a Yo-leven.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          case 6:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Eight.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled a Nine.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Ten.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled a Yo-leven.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled a Boxcars.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          default:
            
            System.out.println("Error: Invalid Answer");
            
          break;
        }
        
      break;
      
      case "no": 
        
        //Randomizing the two die rolls
        System.out.println("Ok, the computer will roll the die for you.");
        dice1 = (int)(Math.random() * 6) + 1;
        dice2 = (int)(Math.random() * 6) + 1;
        
        //Checking if dice1 is a valid roll
        switch (dice1) {
            
          case 1:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled a Snake Eyes.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled an Ace Deuce.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Four.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled a Fever Five.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Six.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
          
          case 2:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled an Ace Deuce.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled a Hard Four");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled a Fever Five.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Six.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Eight.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          case 3:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled an Easy Four.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled a Fever Five.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled a Hard Six.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Eight.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled a Nine.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          case 4:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled a Fever Five,");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Six.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled a Hard Eight.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled a Nine.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Ten.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          case 5:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled an Easy Six.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Eight.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled a Nine.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled a Hard Ten.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled a Yo-leven.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          case 6:
            
            //Checking if dice2 is a valid roll
            switch (dice2) {
                
              case 1:
                
                //Outputting the slang term
                System.out.println("You rolled a Seven Out.");
                
              break;
                
              case 2:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Eight.");
            
              break;
            
              case 3:
            
                //Outputting the slang term
                System.out.println("You rolled a Nine.");
            
              break;
            
              case 4:
            
                //Outputting the slang term
                System.out.println("You rolled an Easy Ten.");
            
              break;
            
              case 5:
            
                //Outputting the slang term
                System.out.println("You rolled a Yo-leven.");
            
              break;
            
              case 6:
            
                //Outputting the slang term
                System.out.println("You rolled a Boxcars.");
            
              break;
            
              default:
            
                //If the answer is invalid
                System.out.println("Error: Invalid Answer");
            
              break;
            }
            
          break;
            
          default:
            
            System.out.println("Error: Invalid Answer");
            
          break;
        }
        
      break;    
      
      default: 
               break;
    
    }
    
    
        
  }
  
}