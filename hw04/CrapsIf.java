//Alexander Carr CSE 002
//September 20 2018
//CrapsIf is a program that either randomly chooses two die rolls or receives input for two die rolls and outputs the unique Craps name for the die combination (using only if statements)

import java.util.Scanner;

public class CrapsIf {
  
  public static void main(String[] args) {
    
    //Initializing scanner
    Scanner sc = new Scanner(System.in);
    
    //Declaring variables
    String input;
    int dice1;
    int dice2;
    
    //Obtaining the input for either a random roll or a user generated roll
    System.out.println("Would you like to roll the die? (Enter 'yes' or 'no')");
    input = sc.nextLine();
    
    //Checking if the user chose to roll the die themselves
    if (input.equals("yes")) {
      
      //Obtaining the input of the two die from the user
      System.out.println("Ok, what 2 numbers did you roll? (Enter a number between 1 and 6 for each roll)");
      dice1 = sc.nextInt();
      dice2 = sc.nextInt();
      
      //Checking if the user inputted a valid die number
      if (dice1 >= 1 && dice1 <= 6 && dice2 >= 1 && dice2 <= 6) {
        
        //If the total value on the die is 2
        if (dice1 + dice2 == 2) {
          
          //Outputting the slang
          System.out.println("You rolled a Snake Eyes.");
          
        }
        
        //If the total value on the die is 3
        if (dice1 + dice2 == 3) {
          
          //Outputting the slang
          System.out.println("You rolled an Ace Duece.");
          
        }
        
        //If the total value on the die is 4
        if (dice1 + dice2 == 4) {
          
          //Checking for a hard four
          if (dice1 == 2 && dice2 == 2) {
            
            //Outputting the slang
            System.out.println("You rolled a Hard Four.");
            
          }
          
          //If the roll is not a hard four
          else {
            
            System.out.println("You rolled an Easy Four.");
            
          }
          
        }
        
        //If the total value on the die is 5
        if (dice1 + dice2 == 5) {
          
          //Outputting the slang
          System.out.println("You rolled a Fever Five.");
          
        }
        
        //If the total value on the die is 6
        if (dice1 + dice2 == 6) {
          
          //Checking for a hard six
          if (dice1 == 3 && dice2 == 3) {
            
            //Outputting the slang
            System.out.println("You rolled a Hard Six.");
            
          }
          
          //If the roll is not a hard six
          else {
            
            System.out.println("You rolled an Easy Six.");
            
          }
          
        }
        
        //If the total value on the die is 7
        if (dice1 + dice2 == 7) {
          
          //Outputting the slang
          System.out.println("You rolled a Seven Out.");
          
        }
        
        //If the total value on the die is 8
        if (dice1 + dice2 == 8) {
          
          //Checking for a hard eight
          if (dice1 == 4 && dice2 == 4) {
            
            //Outputting the slang
            System.out.println("You rolled a Hard Eight.");
            
          }
          
          //If the roll is not a hard eight
          else {
            
            System.out.println("You rolled an Easy Eight.");
            
          }
          
        }
        
        //If the total value on the die is 9
        if (dice1 + dice2 == 9) {
          
          //Outputting the slang
          System.out.println("You rolled a nine.");
          
        }
        
        //If the total value on the die is 10
        if (dice1 + dice2 == 10) {
          
          //Checking for a hard ten
          if (dice1 == 5 && dice2 == 5) {
            
            //Outputting the slang
            System.out.println("You rolled a Hard Ten.");
            
          }
          
          //If the roll is not a hard ten
          else {
            
            System.out.println("You rolled an Easy Ten.");
            
          }
          
        }
        
        //If the total value on the die is 11
        if (dice1 + dice2 == 11) {
          
          //Outputting the slang
          System.out.println("You rolled a Yo-leven.");
          
        }
        
        //If the total value on the die is 12
        if (dice1 + dice2 == 12) {
          
          //Outputting the slang
          System.out.println("You rolled a Boxcars.");
          
        }
        
      }
      
      //If the user enters an invalid number
      else {
        
        System.out.println("Error: Invalid Answer");
        
      }
      
    }
    
    //Checking if the user chose to randomly roll the die
    else if (input.equals("no")) {
      
      //Randomizing the two die rolls
      System.out.println("Ok, the computer will roll the die for you.");
      dice1 = (int)(Math.random() * 6) + 1;
      dice2 = (int)(Math.random() * 6) + 1;
      
      //If the total value on the die is 2
        if (dice1 + dice2 == 2) {
          
          //Outputting the slang
          System.out.println("You rolled a Snake Eyes.");
          
        }
        
        //If the total value on the die is 3
        if (dice1 + dice2 == 3) {
          
          //Outputting the slang
          System.out.println("You rolled an Ace Duece.");
          
        }
        
        //If the total value on the die is 4
        if (dice1 + dice2 == 4) {
          
          //Checking for a hard four
          if (dice1 == 2 && dice2 == 2) {
            
            //Outputting the slang
            System.out.println("You rolled a Hard Four.");
            
          }
          
          //If the roll is not a hard four
          else {
            
            System.out.println("You rolled an Easy Four.");
            
          }
          
        }
        
        //If the total value on the die is 5
        if (dice1 + dice2 == 5) {
          
          //Outputting the slang
          System.out.println("You rolled a Fever Five.");
          
        }
        
        //If the total value on the die is 6
        if (dice1 + dice2 == 6) {
          
          //Checking for a hard six
          if (dice1 == 3 && dice2 == 3) {
            
            //Outputting the slang
            System.out.println("You rolled a Hard Six.");
            
          }
          
          //If the roll is not a hard six
          else {
            
            System.out.println("You rolled an Easy Six.");
            
          }
          
        }
        
        //If the total value on the die is 7
        if (dice1 + dice2 == 7) {
          
          //Outputting the slang
          System.out.println("You rolled a Seven Out.");
          
        }
        
        //If the total value on the die is 8
        if (dice1 + dice2 == 8) {
          
          //Checking for a hard eight
          if (dice1 == 4 && dice2 == 4) {
            
            //Outputting the slang
            System.out.println("You rolled a Hard Eight.");
            
          }
          
          //If the roll is not a hard eight
          else {
            
            System.out.println("You rolled an Easy Eight.");
            
          }
          
        }
        
        //If the total value on the die is 9
        if (dice1 + dice2 == 9) {
          
          //Outputting the slang
          System.out.println("You rolled a nine.");
          
        }
        
        //If the total value on the die is 10
        if (dice1 + dice2 == 10) {
          
          //Checking for a hard ten
          if (dice1 == 5 && dice2 == 5) {
            
            //Outputting the slang
            System.out.println("You rolled a Hard Ten.");
            
          }
          
          //If the roll is not a hard ten
          else {
            
            System.out.println("You rolled an Easy Ten.");
            
          }
          
        }
        
        //If the total value on the die is 11
        if (dice1 + dice2 == 11) {
          
          //Outputting the slang
          System.out.println("You rolled a Yo-leven.");
          
        }
        
        //If the total value on the die is 12
        if (dice1 + dice2 == 12) {
          
          //Outputting the slang
          System.out.println("You rolled a Boxcars.");
          
        }
      
    }
    
    //If the user enters an invalid phrase
    else {
      
      System.out.println("Error: Invalid Answer");
      
    }
    
  }
  
}