//Alexander Carr CSE 002
//1 December 2018
//HW10 is a program that simulates the game of Tic Tac Toe, including displaying the board and prompting each player for a move.

import java.util.*;

public class HW10 {
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		Scanner sc = new Scanner(System.in);
		String[][] board = {{"1", "2", "3"}, {"4", "5", "6"}, {"7", "8", "9"}};
		int input;
		int win = 0, turn = 0;
		String pos;
		String insert = "";
		boolean taken = true;
		boolean checkType;
		
		//Looping through the turns
		while (win == 0) {
			
			//Printing the board
			print(board);
			
			//Checking if it is player one's turn
			if (turn % 2 == 0) {
				
				//Prompting player 1 for an input
				System.out.println("Player 1, please place your X on the board (enter a number between 1 and 9):");
				insert = "X";
				
			}
			
			//Checking if it is player two's turn
			else if (turn % 2 == 1) {
				
				//Prompting player 2 for an input
				System.out.println("Player 2, please place your O on the board (board a number between 1 and 9):");
				insert = "O";
				
			}
			
			//Checking if the user entered an int
			checkType = sc.hasNextInt();
			
			//If the user did enter an int
			if (checkType == true) {
				
				//Obtaining the user's input
				input = sc.nextInt();
				
				//Checking if the input is between 1 and 9
				if (input >= 1 && input <= 9) {
					
					//Converting the input to a string
					pos = intToString(input);
					
					//Cycling through the rows of the board
					for (int i = 0; i < 3; i++) {
						
						//Cycling through the columns of the board
						for (int j = 0; j < 3; j++) {
							
							//Checking if the user entered the number of an open space
							if (board[i][j].equals(pos)) {
								
								//Placing the X or O on the board and printing the board
								board[i][j] = insert;
								win = checkWin(board);
								taken = false;
								break;
								
							}
							
						}
						
					}
					
					//If the user did not enter the number of an open space
					if (taken == true) {
						
						//Letting the user know that they made an error
						System.out.println("Error: Position already taken");
						turn--;
						
					}
					
				}
				
				//If the input is not between 1 and 9
				else {
					
					//Letting the user know that they made an error
					System.out.println("Error: Integer out of range");
					turn--;
					
				}
				
			}
			
			//If the user did not enter an int
			else if (checkType == false) {
				
				//Letting the user know that they made an error
				System.out.println("Error: Not an integer");
				sc.nextLine();
				turn--;
				
			}
			
			//Checking if the game ended in a draw
			if (turn == 8 && win == 0) {
				
				//Printing that the game ended in a draw
				print(board);
				System.out.println("DRAW");
				break;
				
			}
			
			//Checking if X won
			if (win == 1) {
				
				//Printing that X wins
				print(board);
				System.out.println("X WINS");
				
			}
			
			//Checking if O won
			if (win == 2) {
				
				//Printing that O wins
				print(board);
				System.out.println("O WINS");
				
			}
			
			//Incrementing the turn and resetting other variables
			taken = true;
			turn++;
			
		}
		
	} //End of main method
	
	public static int checkWin(String[][] array) {
		
		//Declaring and initializing variables
		int output = 0, countX = 0, countO = 0, counter = 2;
		
		//Using nested for loops to check the rows
		for (int i = 0; i < array.length; i++) {
			
			//Cycling through the columns
			for (int j = 0; j < array[i].length; j++) {
				
				//Finding how many X's are in each row
				if (array[i][j].equals("X")) {
					
					//Incrementing the X counter
					countX++;
					
				}
				
				//Finding how many O's are in each row
				if (array[i][j].equals("O")) {
					
					//Incrementing the O counter
					countO++;
					
				}
				
			}
			
			//Checking if X won
			if (countX == 3) {
				
				//Setting the output
				output = 1;
				break;
				
			}
			
			//Checking if O won
			if (countO == 3) {
				
				//Setting the output
				output = 2;
				break;
				
			}
			
			//Resetting variables
			countX = 0;
			countO = 0;
			
		}
		
		//Checking if someone has already won
		if (output == 0) {
			
			//Using nested for loops to check the columns
			for (int i = 0; i < array[0].length; i++) {
				
				//Cycling through the rows
				for (int j = 0; j < array.length; j++) {
					
					//Finding how many X's are in each row
					if (array[j][i].equals("X")) {
						
						//Incrementing the X counter
						countX++;
						
					}
					
					//Finding how many O's are in each row
					if (array[i][j].equals("O")) {
						
						//Incrementing the O counter
						countO++;
						
					}
					
				}
				
				//Checking if X won
				if (countX == 3) {
					
					//Setting the output
					output = 1;
					break;
					
				}
				
				//Checking if O won
				if (countO == 3) {
					
					//Setting the output
					output = 2;
					break;
					
				}
				
				//Resetting variables
				countX = 0;
				countO = 0;
				
			}
			
		}
		
		//Checking if someone has already won
		if (output == 0) {
			
			//Cycling through the diagonal
			for (int i = 0; i < array.length; i++) {
				
				//Finding how many X's are in each row
				if (array[i][i].equals("X")) {
					
					//Incrementing the X counter
					countX++;
					
				}
				
				//Finding how many O's are in each row
				if (array[i][i].equals("O")) {
					
					//Incrementing the O counter
					countO++;
					
				}
				
			}
			
			//Checking if X won
			if (countX == 3) {
				
				//Setting the output
				output = 1;
				
			}
			
			//Checking if O won
			if (countO == 3) {
				
				//Setting the output
				output = 2;
				
			}
			
			//Resetting variables
			countX = 0;
			countO = 0;
			
		}
		
		//Checking if someone has already won
		if (output == 0) {
			
			//Cycling through the diagonal
			for (int i = 0; i < array.length; i++) {
				
				//Finding how many X's are in each row
				if (array[i][counter].equals("X")) {
					
					//Incrementing the X counter
					countX++;
					
				}
				
				//Finding how many O's are in each row
				if (array[i][counter].equals("O")) {
					
					//Incrementing the O counter
					countO++;
					
				}
				
				//Decrementing the counter
				counter--;
				
			}
			
			//Checking if X won
			if (countX == 3) {
				
				//Setting the output
				output = 1;
				
			}
			
			//Checking if O won
			if (countO == 3) {
				
				//Setting the output
				output = 2;
				
			}
			
			//Resetting variables
			countX = 0;
			countO = 0;
			counter = 2;
			
		}
		
		//Returning the output
		return output;
		
	} //End of checkWin method
	
	public static String intToString(int input) {
		
		//Declaring and initializing variables
		String output = "";
		
		//Using a switch statement to check what the value of input is
		switch (input) {
		
		//Checking each integer between 1 and 9
		case 1: output = "1";
				break;
		case 2: output = "2";
				break;
		case 3: output = "3";
				break;
		case 4: output = "4";
				break;
		case 5: output = "5";
				break;
		case 6: output = "6";
				break;
		case 7: output = "7";
				break;
		case 8: output = "8";
				break;
		case 9: output = "9";
				break;
		
		}
		
		//Returning the output
		return output;
		
	} //End of intToString method
	
	public static void print(String[][] array) {
		
		//Cycling through each row
		for (int i = 0; i < array.length; i++) {
			
			//Cycling through each column
			for (int j = 0; j < array[i].length; j++) {
				
				//Printing the array
				System.out.print(array[i][j] + " ");
				
			}
			
			//Creating a new line
			System.out.println();
			
		}
		
	} //End of print method

}