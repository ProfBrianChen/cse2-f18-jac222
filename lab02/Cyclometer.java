//Alexander Carr CSE 002
//Sep 6 2018
//Cyclometer program that calculates time in minutes and distance in miles when given seconds and wheel rotations from a bike ride

public class Cyclometer {
  
  public static void main(String[] args) {
    
    int secsTrip1 = 480; //The amount of seconds for trip 1
    int secsTrip2 = 3220; //The amount of seconds for trip 2
    int countsTrip1 = 1561; //The amount of rotation counts for trip 1
    int countsTrip2 = 9037; //The amount of rotation counts for trip 2
    
    double wheelDiameter = 27.0; //The diameter of the wheel of the bike
    double PI = 3.14159; //The value of pi
    double feetPerMile = 5280.0; //The amount of feet in a mile
    double inchesPerFoot = 12.0; //The amount of inches in a foot
    double secondsPerMinute = 60.0; //The amount of seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //The variables for the distance in miles of trip 1 and trip 2 and the total distance of trip 1 and trip 2 combined
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); //Outputting the time in minutes and rotation counts for trip 1
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //Outputting the time in minutes and rotation counts for trip 2
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //Calculating the distance of trip 1 in inches
    distanceTrip1 /= inchesPerFoot * feetPerMile; //Calculating the distance of trip 1 in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI; //Calculating the distance of trip 2 in inches
    distanceTrip2 /= inchesPerFoot * feetPerMile; //Calculating the distance of trip 2 in miles
    totalDistance = distanceTrip1 + distanceTrip2; //Calculating the total distance of the two trips combined
    
    System.out.println("Trip 1 was " + distanceTrip1 + " miles."); //Outputting the distance in miles of trip 1
    System.out.println("Trip 2 was " + distanceTrip2 + " miles."); //Outputting the distance in miles of trip 2
    System.out.println("The total distance was " + totalDistance + " miles."); //Outputting the total distance in miles of trip 1 and trip 2 combined
    
  } //End of main method
  
} //End of class