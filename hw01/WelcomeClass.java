//Alexander Carr
//CSE002 HW01

public class WelcomeClass{
  
  public static void main(String args[]){
    
    //Printing the WELCOME line
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    
    //Printing my Username
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--A--C--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    
    //Printing my bio
    System.out.println("My name is Alexander Carr and I am a first year student");
    System.out.println("at Lehigh University. I am in the Integrated Business");
    System.out.println("and Engineering Program and I will be majoring in");
    System.out.println("Finance and Computer Engineering. I am excited to learn");
    System.out.println("how to program in CSE002!");
    
  }
  
}