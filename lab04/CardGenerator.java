//Alexander Carr CSE 002
//September 20 2018
//Card generator is a program that generates a random card from a standard deck of cards

public class CardGenerator {
  
  public static void main(String[] args) {
    
    //Declaring and initializing the variables needed in the program
    String suitType = " ";
    String cardType = " ";
    int randNum;
    
    //Generating a random number
    randNum = (int)(Math.random() * 52) + 1;
    
    //Checking if the random card is a diamond
    if (randNum >= 1 && randNum <= 13) {
      
      //Assigning suitType
      suitType = "Diamonds";
            
    }
    
    //Checking if the random card is a club
    if (randNum >= 14 && randNum <= 26) {
      
      //Assigning suitType
      suitType = "Clubs";
      
    }
    
    //Checking if the random card is a heart
    if (randNum >= 27 && randNum <= 39) {
      
      //Assigning suitType
      suitType = "Hearts";
      
    }
    
    //Checking if the random card is a spade
    if (randNum >= 40 && randNum <= 52) {
      
      //Assigning suitType
      suitType = "Spades";
          
      }
    
    //Reducing the random card to a number between 0 and 12
    randNum = randNum % 13;
    
    //Checking what type of card it is
    switch (randNum) {
          
      case 0: cardType = "King";
              break;
      case 1: cardType = "Ace";
              break;
      case 2: cardType = "2";
              break;
      case 3: cardType = "3";
              break;
      case 4: cardType = "4";
              break;
      case 5: cardType = "5";
              break;
      case 6: cardType = "6";
              break;
      case 7: cardType = "7";
              break;
      case 8: cardType = "8";
              break;
      case 9: cardType = "9";
              break;
      case 10: cardType = "10";
              break;
      case 11: cardType = "Jack";
              break;
      case 12: cardType = "Queen";
                break;
          
      }
    
    //Outputting the randomly chosen card
    System.out.println("You picked the " + cardType + " of " + suitType);
    
  }
  
}