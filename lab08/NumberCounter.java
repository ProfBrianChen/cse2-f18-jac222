//Alexander Carr CSE 002
//8 November 2018
//NumberCounter is a program that randomly fills a 1 x 100 array with numbers and counts the number of times each number occurs.

import java.util.Arrays;

public class NumberCounter {
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		int randomArray[] = new int[100];
		int numCount[] = new int[100];
		int counter;
		
		//Using a for loop to fill randomArray with random numbers
		for (int i = 0; i < 100; i++) {
			
			//Storing the random value in randomArray
			randomArray[i] = (int)(Math.random() * 99);

		}
		
		//Using a for loop to count the values
		for (int j = 0; j < 100; j++) {
			
			//Resetting the counter
			counter = 0;
			
			//Using a nested for loop to check each value in randomArray with numArray
			for (int k = 0; k < 100; k++) {
				
				//Checking if the randomArray is equal to the numArray
				if (j == randomArray[k]) {
					
					//Incrementing the counter
					counter++;
					
				}
				
			}
			
			//Storing the times each number appears
			numCount[j] = counter;
			
		}
		
		//Outputting the randomArray
		System.out.println("Array 1 holds the following integers: ");
		for (int l = 0; l < 100; l++) {
			
			//Outputting the randomArray
			System.out.print(randomArray[l] + " ");
			
		}
		System.out.println("");
		
		//Using a for loop to output the number of times each number appears in randomArray
		for (int m = 0; m < 100; m++) {
			
			//Checking for singular vs plural
			if (numCount[m] == 1) {
				
				//Outputting the number of times
				System.out.println(m + " occurs " + numCount[m] + " time");
				
			}
			
			//Checking for singular vs plural
			if (numCount[m] != 1) {
				
				//Outputting the number of times
				System.out.println(m + " occurs " + numCount[m] + " times");
				
			}
			
		}
		
	} //End of main method

}