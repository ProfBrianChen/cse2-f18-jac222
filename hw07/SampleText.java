//Alexander Carr CSE 002
//25 October 2018
//SampleText is a program that takes an excerpt of sample text and outputs the number of words, characters, spaces, and more.

import java.util.Scanner;

public class SampleText {
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		Scanner sc = new Scanner(System.in);
		String sample, choice, findWord;
		boolean check = false;
		
		//Prompting the user to enter a sample text
		System.out.println("Enter a sample text:");
		sample = sc.nextLine();
		
		//Outputting the sample text
		System.out.println("You entered:\n" + sample);
		
		//Checking the user's input
		do {
			
			//Obtaining the user's input
			choice = printMenu(sample, sc);
			
			//Checking for c
			if (choice.equals("c")) {
				
				//Printing the number of non-whitespace characters
				System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(sample));
				
			}
			
			//Checking for w
			else if (choice.equals("w")) {
				
				//Printing the number of words
				System.out.println("Number of words: " + getNumOfWords(sample));
				
			}
			
			//Checking for f
			else if (choice.equals("f")) {
				
				//Prompting the user for the word to find
				System.out.println("Enter a word or phrase to be found:");
				findWord = sc.next();
				
				//Printing the number of the words in the sample text
				System.out.println("\"" + findWord + "\" instances: " + findText(findWord, sample));
				
			}
			
			//Checking for r
			else if (choice.equals("r")) {
				
				//Printing the edited text with the "!" replaced
				System.out.println("Edited text:\n" + replaceExclamation(sample));
				
			}
			
			//Running shortenSpace method
			else if (choice.equals("s")) {
				
				//Printing out the edited text with no double spaces
				System.out.println("Edited text:\n" + shortenSpace(sample));
				
			}
			
			//Checking for q
			else if (choice.equals("q")) {
				
				//Exiting the loop
				System.out.println("Quitting...");
				check = true;
				
			}
			
			//Checking for a wrong input
			else {
				
				//Asking for a new input
				System.out.println("Error: invalid input\nPlease enter a character from the menu:");
				sc.nextLine();
				
			}
			
		} while (check == false);
		
		//Quitting the program
		System.out.println("Quit");
		
	} //End of main method
	
	public static String printMenu(String sample, Scanner sc) {
		
		//Declaring and initializing variables
		String choice;
		
		//Printing the menu
		System.out.println("MENU");
		System.out.println("c -- Number of non-whitespace characters");
		System.out.println("w -- Number of words");
		System.out.println("f -- Find text");
		System.out.println("r -- Replace all !'s");
		System.out.println("s -- Shorten spaces");
		System.out.println("q -- Quit\n");
		System.out.println("Choose an option:");
			
		//Obtaining the user's input
		choice = sc.next();
		
		//Returning the output
		return choice;
		
	} //End of printMenu method
	
	public static int getNumOfNonWSCharacters(String sample) {
		
		//Declaring and initializing variables
		int nonWSCharacters;
		
		//Removing all the spaces in the sample text
		sample = sample.replaceAll("\\s","");
		nonWSCharacters = sample.length();
		
		//Returning the output
		return nonWSCharacters;
		
	} //End of getNumOfNonWSCharacters method
	
	public static int getNumOfWords(String sample) {
		
		//Declaring and initializing variables
		int strStart = 0, check = 0, numWord = 0;
		
		//Removing extra spaces at the beginning and end of the string
		sample = sample.trim();
		
		//Replacing all of the periods and tabs with spaces for easier word counting
		sample = sample.replaceAll("\\.", " ");
		sample = sample.replaceAll("\t", " ");
		
		//Replacing all of the double spaces with a single space
		for (int i = 0; i < sample.length(); i++) {
			
			//Replacing all the double spaces with a single space
			sample = sample.replaceAll("  ", " ");
			
		}
		
		//Using a while loop to count the number of words are in the sample text
		while (check != -1) {
				
			//Checking if there is a space in the sample text
			check = sample.indexOf(" ", strStart);
				
			//Setting the new start point for the string
			strStart = check + 1;
				
			//Checking if check is not -1
			if (check != -1) {
					
				//Incrementing the word counter
				numWord++;
					
			}
					
		}
		
		//Returning the output
		return numWord + 1;
		
	} //End of getNumOfWords method
	
	public static int findText(String findWord, String sample) {
		
		//Declaring and initializing variables
		int wordNum = 0, check = 0, strStart = 0;
		int lengthWord;
		
		//Setting the length of the string
		lengthWord = findWord.length();
		
		//Using a while loop to count the number of times the word is in the sample text
		while (check != -1) {
			
			//Checking if the word is in the sample text
			check = sample.indexOf(findWord, strStart);
			
			//Setting the new start point for the string
			strStart = check + lengthWord;
			
			//Checking if check is not -1
			if (check != -1) {
				
				//Incrementing the word counter
				wordNum++;
				
			}
			
		}
		
		//Returning the output
		return wordNum;
		
	} //End of findText method
	
	public static String replaceExclamation(String sample) {
		
		//Declaring and initializing variables
		String editedString;
		
		//Replacing all of the exclamation points with periods
		editedString = sample.replaceAll("!", "\\.");
		
		//Returning the output
		return editedString;
		
	} //End of replaceExclamation method
	
	public static String shortenSpace(String sample) {
		
		//FIXME
		//Declaring and initializing variables
		String editedString = "";
				
		//Removing extra spaces at the beginning and end of the string
		sample = sample.trim();
		
		//Replacing all of the tabs with spaces for easier word counting
		sample = sample.replaceAll("\t", " ");
		
		//Replacing all of the double spaces with a single space
		for (int i = 0; i < sample.length(); i++) {
			
			//Replacing all the double spaces with a single space
			editedString = sample.replaceAll("  ", " ");
			
		}
		
		//Returning the output
		return editedString;
		
	} //End of shortenSpace method

} //End of class