//Alexander Carr CSE 002
//Sep 6 2018
//Arithmetic is a program that calculates the total cost and sales tax for 3 types of
//clothing and the total cost, sales tax, and payment due for all of the clothing.

public class Arithmetic {
  
  public static void main(String[] args) {
    
    int numPants = 3; //Number of pairs of pants
    int numShirts = 2; //Number of shirts
    int numBelts = 1; //Number of belts
    
    double pantsPrice = 34.98; //Cost per pants
    double shirtPrice = 24.99; //Cost per shirt
    double beltCost = 33.99; //Cost per belt
    double paSalesTax = 0.06; //The tax rate in PA
    double totalCostPants, totalCostShirts, totalCostBelt; //The total cost of each type of item before tax
    double salesTaxPants, salesTaxShirts, salesTaxBelt; //The total sales tax for each type of item
    double totalCost; //The total cost before tax of all of the items
    double totalSalesTax; //The total sales tax on all of the items
    double totalPayment; //The total amount paid after tax to purchase all of these items
    
    totalCostPants = numPants * pantsPrice; //Calculating the total cost of the pants
    totalCostShirts = numShirts * shirtPrice; //Calculating the total cost of the shirts
    totalCostBelt = numBelts * beltCost; //Calculating the total cost of the belt
    
    totalCostPants = (double)((int)(totalCostPants * 100)) / 100; //Reducing the total cost of the pants to 2 decimal places
    totalCostShirts = (double)((int)(totalCostShirts * 100)) / 100; //Reducing the total cost of the shirts to 2 decimal places
    totalCostBelt = (double)((int)(totalCostBelt * 100)) / 100; //Reducing the total cost of the belt to 2 decimal places
    
    System.out.println("The total cost for the pants is $" + totalCostPants + "."); //Outputting the total cost of the pants
    System.out.println("The total cost for the shirts is $" + totalCostShirts + "."); //Outputting the total cost of the shirts
    System.out.println("The total cost for the belt is $" + totalCostBelt + "."); //Outputting the total cost of the belt
    
    salesTaxPants = totalCostPants * paSalesTax; //Calculating the sales tax on the pants
    salesTaxShirts = totalCostShirts * paSalesTax; //Calculating the sales tax on the shirts
    salesTaxBelt = totalCostBelt * paSalesTax; //Calculating the sales tax on the belt
    
    salesTaxPants = (double)((int)(salesTaxPants * 100)) / 100; //Reducing the sales tax of the pants to 2 decimanl places
    salesTaxShirts = (double)((int)(salesTaxShirts * 100)) / 100; //Reducing the sales tax of the shirts to 2 decimal places
    salesTaxBelt = (double)((int)(salesTaxBelt * 100)) / 100; //Reducing the sales tax of the belt to 2 decimal places
    
    System.out.println("The sales tax on the pants is $" + salesTaxPants + "."); //Outputting the sales tax of the pants
    System.out.println("The sales tax on the shirts is $" + salesTaxShirts + "."); //Outputting the sales tax of the shirts
    System.out.println("The sales tax on the belt is $" + salesTaxBelt + "."); //Outputting the sales tax of the belt
    
    totalCost = totalCostPants + totalCostShirts + totalCostBelt; //Calculating the total cost of all the items
    totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelt; //Calculating the total sales tax from all the items
    
    System.out.println("The total cost of all the items is $" + totalCost + "."); //Outputting the total cost of all the items
    System.out.println("The total sales tax of all the items is $" + totalSalesTax + "."); //Outputting the total sales tax from all the items
    
    totalPayment = totalCost + totalSalesTax; //Calculating the total payment due for the transaction
    
    System.out.println("The total payment due is $" + totalPayment + "."); //Outputting the total payment due for the transaction
        
  }//End of main method
  
}//End of class