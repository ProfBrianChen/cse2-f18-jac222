//Alexander Carr CSE 002
//October 17 2018
//Encrypted X is a program that creates a square with a user's side length of characters that has a hidden X inside.

import java.util.Scanner;

public class EncryptedX {
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		Scanner sc = new  Scanner(System.in);
		int rows = 0, counter = 1;
		boolean checkType;
		String output = "";
		
		//Asking the user for the number of rows
		System.out.println("What is the side length of your square (enter an integer between 0 and 100)?");
		do {
			
			//Checking if the user inputs an int
			checkType = sc.hasNextInt();
			
			//If the user did not enter an integer
			if (checkType == false) {
				
				//Asking for another input
				sc.next();
				System.out.println("Error: Please enter another integer");
				
			}
			
			//If the user did enter an integer
			if (checkType == true) {
				
				//Obtaining the correct input
				rows = sc.nextInt();
				
				//Checking if the integer is between 1 and 5
				if (rows < 0 || rows > 100) {
					
					//Asking for another input
					System.out.println("Error: Please enter another integer");
					checkType = false;
					
				}
				
			}
			
		} while (checkType == false);
		
		//Creating a for loop to create the square
		for (int i = 1; i <= rows; i++) {
			
			//Clearing the output string before each row
			output = "";
			
			//Using another for loop to create the rows of the square
			for (int j = 1; j <= rows; j++) {
				
				//Creating the spaces to create the x in the square
				if (j == counter || j == rows - counter + 1) {
					
					//Adding a space to the end of output
					output = output + " ";
					
				}
				
				//Creating the stars to fill in the square
				else {
					
					//Adding a star to the end of output
					output = output + "*";
					
				}
				
			}
			
			//Incrementing the counter
			counter++;
			
			//Outputting the square
			System.out.println(output);
			
		}
		
	}

}