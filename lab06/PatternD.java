//Alexander Carr CSE 002
//October 11 2018
//PatternD is a program that creates a pattern with the number of rows that are inputted by the user.

import java.util.Scanner;

public class PatternD {
	
	public static void main(String[] args) {
		
		//Initializing and declaring variables
		Scanner sc = new Scanner(System.in);
		int numRows = 0;
		boolean checkType;
		
		//Getting the input for the number of rows
		System.out.println("Please input an integer between 1 and 10:");
	    
	    //Using a do while loop to continually check for the correct input
	    do {
	      
	      //Checking that the user inputs the correct type
	      checkType = sc.hasNextInt();
	      
	      //CHecking if the input is an int
	      if (checkType == true) {
	        
	        //Obtaining the correct input
	        numRows = sc.nextInt();
	        
	      }
	      
	      //Checking if the input is not an int
	      if (checkType == false) {
	        
	        //Clearing the incorrect input and prompting the user for another input
	        sc.next();
	        System.out.println("Error: Invalid Input\nPlease enter an int:");
	        
	      }
	            
	    } while (checkType == false);
		
	    //Creating nested for loops to make the pyramid
		for (int i = 1; i <= numRows; i++) {
			
			//Initializing a column number
			int colNum = numRows + 1 - i;
			
			//Created a second loop to output the rows of the pyramid
			for (int j = numRows + 1 - i; j > 0; j--) {
				
				//Outputting the pyramid
				System.out.print(colNum + " ");
				colNum--;
				
			}
			
			//Creating a new line
			System.out.println("");
			
		}		
		
	}

}
