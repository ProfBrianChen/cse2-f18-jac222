//Alexander Carr CSE 002
//6 December 2018
//SelectionSort is a program that counts the number of operations required when sorting arrays by selection.

import java.util.*;

public class SelectionSort {
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		
		//Outputting the number of operations required for each array
		int iterBest = selectionSort(myArrayBest);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
		
	} //End of main method
	
	public static int selectionSort(int[] list) {
		
		//Declaring and initializing variables
		int iterations = 0;
		
		//Printing the array
		System.out.println(Arrays.toString(list));
		
		//Using a for loop to sort the array
		for (int i = 0; i < list.length - 1; i++) {
			
			//Incrementing the iterations
			iterations++;
			
			//Initializing the minimum value
			int currentMin = list[i];
			int currentMinIndex = i;
			
			//Using a for loop to check all of the other values in the array
			for (int j = i + 1; j < list.length; j++) {
				
				//Incrementing the iterations
				iterations++;
				
				//Checking if the new value is less than the current minimum value
				if (list[j] < currentMin) {
					
					//Creating a new minimum value
					currentMin = list[j];
					currentMinIndex = j;
					
				}
				
			}
			
			//Swapping the old minimum with the new minimum
			if (currentMinIndex != i) {
				
				//Swapping the minimum values
				int temp = list[i];
				list[i] = currentMin;
				list[currentMinIndex] = temp;
				
				//Outputting the array
				System.out.println(Arrays.toString(list));
				
			}
			
		}
		
		//Returning the output
		return iterations;
		
	} //End of selectionSort method

}