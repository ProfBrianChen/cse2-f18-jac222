//Alexander Carr CSE 002
//6 December 2018
//InsertionSort is a program that counts the number of operations required when sorting arrays by insertion.

import java.util.*;

public class InsertionSort {
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
				
		//Outputting the number of operations required for each array
		int iterBest = insertionSort(myArrayBest);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		int iterWorst = insertionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
		
	} //End of main method
	
	public static int insertionSort(int[] list) {
		
		//Declaring and initializing variables
		int iterations = 0;
		
		//Printing the array
		System.out.println(Arrays.toString(list));
		
		//Using a for loop to sort the array
		for (int i = 1; i < list.length; i++) {
			
			//Incrementing the iterations
			iterations++;
			
			//Using a for loop to check all of the other values in the array
			for (int j = i; j > 0; j--) {
				
				//Checking if the new value is less than the old value
				if (list[j] < list[j - 1]) {
					
					//Incrementing iterations
					iterations++;
					
					//Swapping the two values
					int temp = list[j];
					list[j] = list[j - 1];
					list[j - 1] = temp;
					
					//Printing the array
					System.out.println(Arrays.toString(list));
					
				}
				
				else {
					
					//Exiting the loop
					break;
					
				}
				
			}
			
		}
		
		//Returning the output
		return iterations;
		
	} //End of insertionSort method

}