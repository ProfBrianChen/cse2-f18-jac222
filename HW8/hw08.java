//Alexander Carr CSE 002
//12 November 2018
//Shuffling is a program that will shuffle a deck of cards and deal the user a hand.

import java.util.*;

public class hw08{
	
	public static void main(String[] args) {
		
		//Declaring and initializing variables
		Scanner scan = new Scanner(System.in);
		String[] suitNames={"C","H","S","D"};
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
		String[] cards = new String[52];
		String[] hand = new String[5];
		int numCards = 5;
		int again = 1;
		int index = 51;
		
		//Using a for loop to fill in the cards
		for (int i = 0; i < 52; i++){
			
			//Filling each position with a card number and suit and printing it
			cards[i] = rankNames[i % 13] + suitNames[i / 13];
			System.out.print(cards[i] + " ");
			
		}
		
		//Creating a newline
		System.out.println();
		
		//Printing then shuffling the array
		shuffle(cards);
		printArray(cards);
		
		//Using a while to continually as the user for input
		while(again == 1){
			
			//Obtaining and printing the user's hand
			hand = getHand(cards, index, numCards); 
			printArray(hand);
			
			//Subtracting the cards from the index
			index -= numCards;
			
			//Checking if index is negative
			if (index < 0) {
				
				//Resetting index to its original value
				index = 51 - numCards;
				
			}
			
			//Asking the user if they want another hand
			System.out.println("Enter a 1 if you want another hand drawn"); 
			again = scan.nextInt();
			
		}
		
	} //End of main method
	
	public static String[] getHand(String[] cards, int index, int numCards) {
		
		//Declaring and initializing variables
		String[] hand = new String[numCards];
		
		//Checking to make sure there are enough cards to fill the hand array
		if (index >= numCards - 1) {
			
			//Using a for loop to fill the hand array
			for (int i = 0; i < numCards; i++) {
			
				//Filling hand using the end of the cards array
				hand[i] = cards[index - i];
			
			}
		
		}
		
		//If there are not enough cards to fill the hand array
		else {
			
			//Shuffling a new hand
			System.out.println("Not enough cards remaining, shuffling new deck");
			shuffle(cards);
			
			//Using a for loop to fill the hand array
			for (int i = 0; i < numCards; i++) {
			
				//Filling hand using the end of the cards array
				hand[i] = cards[51 - i];
			
			}
			
		}
		
		//Returning the output
		return hand;
		
	} //End of getHand method
	
	public static void shuffle(String[] array) {
		
		//Declaring and initializing variables
		int random, randPos;
		String temp = "";
		
		//Finding a random number of times to shuffle the cards
    	random = (int)(Math.random() * 1000) + 50;
    	
    	//Using a for loop to swap values of the array
    	for (int i = 0; i < random; i++) {
    		
    		//Finding a random position to switch
    		randPos = (int)(Math.random() * 52);
    		
    		//Using a temp value to switch the two values of the array
    		temp = array[0];
    		array[0] = array[randPos];
    		array[randPos] = temp;
    		
    	}
    	
    	//Printing that the cards were shuffled
    	System.out.println("Shuffled");
		
	} //End of shuffle method
	
	public static void printArray(String[] array) {
		
		//Using a for loop to print the array
		for (int i = 0; i < array.length; i++) {
			
			//Checking if it is the last item in the array
			if (i == array.length - 1) {
				
				//Printing the last item in the array
				System.out.print(array[i]);
				
			}
			
			//If it is not the last item in the array
			else {
				
				//Outputting the array
				System.out.print(array[i] + " ");
				
			}
			
		}
		
		//Creating a newline
		System.out.println("");
		
	} //End of printArray method
	
}