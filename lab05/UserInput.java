//Alexander Carr CSE 002
//October 4 2018
//This program asks a student for information about one of their classes, and ensures that everything they enter is a valid input for each category. If they enter an invalid input, it outputs an error message and asks again.

import java.util.Scanner;

public class UserInput {
  
  public static void main(String[] args) {
    
    //Declaring and initializing variables
    Scanner sc = new Scanner(System.in);
    int courseNum = 0, meetWeek = 0, students = 0;
    int clear;
    String depName = " ", time = " ", instructor = " ";
    boolean checkType;
    
    //Asking the user for their course number
    System.out.println("What is the course number for your class?");
    
    //Using a do while loop to continually check for the correct input
    do {
      
      //Checking that the user inputs the correct type
      checkType = sc.hasNextInt();
      
      //CHecking if the input is an int
      if (checkType == true) {
        
        //Obtaining the correct course number
        courseNum = sc.nextInt();
        
      }
      
      //Checking if the input is not an int
      if (checkType == false) {
        
        //Clearing the incorrect input and prompting the user for another input
        sc.next();
        System.out.println("Error: Invalid Input\nPlease enter your course number as an int:");
        
      }
            
    } while (checkType == false);
    
    //Asking the user for their department name
    System.out.println("What is the department name for your class?");
    
    //Obtaining the input for the department name
    depName = sc.next();
    
    //Asking the user for the number of times they meet each week
    System.out.println("What is the amount of times you meet each week for your class?");
    
    //Using a do while loop to continually check for the correct input
    do {
      
      //Checking that the user inputs the correct type
      checkType = sc.hasNextInt();
      
      //CHecking if the input is an int
      if (checkType == true) {
        
        //Obtaining the correct course number
        meetWeek = sc.nextInt();
        
      }
      
      //Checking if the input is not an int
      if (checkType == false) {
        
        //Clearing the incorrect input and prompting the user for another input
        sc.next();
        System.out.println("Error: Invalid Input\nPlease enter your number of meetings each week as an int:");
        
      }
            
    } while (checkType == false);
    
    //Asking the user for the amount of times they meet each week
    System.out.println("What time does your class meet? (Enter as hh:mm in military time)");
    
    //Obtaining the input for the time
    time = sc.next();
    
    //Asking the user for their instructor
    System.out.println("Who is the instructor for your class?");
    
    //Obtaining the correct course number
    instructor = sc.next();
    
    //Asking the user for the number of students in their class
    System.out.println("How many students are in your class?");
    
    //Using a do while loop to coninually check for the correct input
    do {
      
      //Checking that the user inputs the correct type
      checkType = sc.hasNextInt();
      
      //CHecking if the input is an int
      if (checkType == true) {
        
        //Obtaining the correct course number
        students = sc.nextInt();
        
      }
      
      //Checking if the input is not an int
      if (checkType == false) {
        
        //Clearing the incorrect input and prompting the user for another input
        sc.next();
        System.out.println("Error: Invalid Input\nPlease enter the students as an int:");
        
      }
            
    } while (checkType == false);
    
    System.out.println("Your class is " + depName + " " + courseNum + ".  You meet " + meetWeek + " times per week at " + time + ".  The class is taught by Professor " + instructor + " and has " + students + " students.");
    
  }
  
}